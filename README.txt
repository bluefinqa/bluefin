Bluefin - Groovy DSL for Selenium tests
========================================

Write web automation tests easier and quicker using a Selenium aware DSL

Getting started
----------------
- Bluefin requires Java JDK v1.7 or above in order to run. So, make sure 'java -version' works fine.
- Bluefin is a standalone command line application. Once installed you can run it using 'bluefin' command (e.g. bin/bluefin).
- You can execute a single or multiple tests:
	- bluefin path/to/one_test.groovy
	- bluefin path/to/dir_with_tests
	- to override default settings, create 'bluefin.properties' file in the same directory with your tests (same applies when running 1 test)
- To run demo tests execute 'bin/bluefin samples/Demo' 
- Bluefin distribution comes with a few examples, check 'samples' directory for more details.
- See INSTALL.txt for installation instructions.


License
-------
Apache License, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)


Hello World
-----------
start browser
open 'https://google.com/'
assertTitle 'Google'
type 'selenium rocks!', name:'q'
click name:'btnG'
close browser

PS
--
Thank your for trying this project. I hope you'll find it useful. Please send your comments and/or regards to bluefinqa@gmail.com
