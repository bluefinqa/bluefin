start browser

open 'http://www.seleniumhq.org/'
assertTitle 'Selenium - Web Browser Automation'

assertTextPresent 'What is Selenium?'

click linkText:'Download'
assertTitle 'Downloads'

assertTextPresent 'Selenium Client & WebDriver Language Bindings'

close browser