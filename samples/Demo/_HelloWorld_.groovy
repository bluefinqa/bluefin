// Blufin tests are groovy scripts that know about selenium

// One comment line

/*
Multi line 
Comments
Just because we can
*/

/*
 * Comments
 */

println "Groovy rocks!"
println 'And Selenium too!!!'
println ''

(3..7).each {
	println "Iteration: ${it} - Yes, more groovy code..."
}
println ''

println 'Current dir: ' + new File(".").absolutePath
println ''

println 'this.class = ' + this.class
println 'this.toString() = ' + this
println ''

def peeps = [new Person(name:'John', age:33), new Person(name:'SomeOtherCoolName', age:99)]
println "My peeps:"
peeps.each {p ->
	println ' ' + p.name + "${p.age}"
}
println ''

println 'Thanks for trying this!'

// yes, you can stick your classes here too
class Person {
	def name
	int age
}