start browser

open '/page-text.html'

// check for exact match
assertTitle 'Page with Lorem Ipsum content'
assertNotTitle 'Some random text here...'

// check for contains
assertTextPresent 'Praesent id enim at elit facilisis hendrerit.'
assertTextPresent 'enim at elit facilisis'
assertTextNotPresent 'AAAPraesent id enim at elit facilisis hendrerit.AAA'

assertTextPresent 'Maecenas tristique dapibus'
assertTextPresent 'Maecenas'
assertTextNotPresent '___Maecenas'

assertTextNotPresent 'zorro vs. hulk'

assertTextPresent 'Phasellus porttitor nisi id justo tempor, nec laoreet urna gravida.'
assertTextPresent 'porttitor nisi id justo tempor'

// comments should not be found, since they are not visible
assertTextNotPresent 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'

// tags should not be found too, same reason
assertTextNotPresent '<table>'
assertTextNotPresent '<h1>'

close browser