start browser

open '/ajax.html'

assertTitle 'Ajax'

sleep 1000

click id:'btn1'
waitTillElementTextPresent 'Hello world!', id:'results'

click id:'btn2'
waitTillElementPresent id:'hello-cnt'

close browser