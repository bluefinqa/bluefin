start browser

open '/html.html'

assertTitle 'Bluefin Demo HTML - HTML'

assertEquals '<div id="div1">I like <span>HTML</span> and <div>other <span>things</span></div></div>', html(id:'div1')

close browser