package org.bluefinqa.dsl

import static org.junit.Assert.*
import groovy.transform.ToString

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.bluefinqa.dsl.enums.OnOff
import org.bluefinqa.dsl.enums.WebDriverType
import org.bluefinqa.dsl.enums.sets.KeywordSet1
import org.bluefinqa.utils.BluefinLogger
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.ie.InternetExplorerDriver

@ToString(includeNames = true, ignoreNulls = true)
abstract class BaseDsl extends Script {
	WebDriver driver = null
	WebDriverType driverType = WebDriverType.firefox
	ConfigObject config = null
	BluefinLogger log = new BluefinLogger()
	boolean debugMode = false
	CloseableHttpClient httpClient = HttpClients.createDefault()

	//
	// DSL
	//

	void sleep(long ms) {
		logDebug 'Sleeping ' + ms + ' ms...'
		Thread.sleep(ms)
	}

	void start(KeywordSet1 key) {
		logDebug "Starting ${driverType.name}... "
		long ts0 = System.currentTimeMillis()

		if (driverType == WebDriverType.firefox || driverType == WebDriverType.ff) {
			driver = firefox()
		} else if (driverType == WebDriverType.chrome) {
			driver = chrome()
		} else if (driverType == WebDriverType.ie) {
			driver = iexplorer()
		}

		long ts1 = System.currentTimeMillis()
		long e0 = ts1 - ts0

		logDebug "Done! ${driverType.name} started in ${e0} ms."
	}

	void close(KeywordSet1 b) {
		driver?.quit()
		logDebug "${driverType.name} closed."
	}

	void debug(OnOff flag) {
		if (flag == OnOff.on) {
			debugMode = true
		} else if (flag == OnOff.off) {
			debugMode = false
		}
	}

	//
	// Helpers
	//

	void logDebug(String msg) {
		if (debugMode) {
			log.debug msg
		}
	}

	void cleanUp() {
		driver?.quit()
	}

	List<WebElement> findAllElementsBy(By by) {
		assertNotNull "No open browser found", driver
		driver.findElements(by)
	}

	WebElement findOneElementBy(By by) {
		assertNotNull "No open browser found", driver
		def elements = findAllElementsBy(by)
		if (elements) {
			return elements[0]
		}
		null
	}

	//
	// 	Web drivers
	//

	FirefoxDriver firefox() {
		FirefoxProfile fp = new FirefoxProfile()
		FirefoxDriver ff = new FirefoxDriver(fp)
		return ff
	}

	ChromeDriver chrome() {
		//		System.setProperty("webdriver.chrome.driver", "C:\\devapps\\chromedriver\\chromedriver.exe")
		//		return new ChromeDriver()
		throw new UnsupportedOperationException("Chrome is not supported yet...")
	}

	InternetExplorerDriver iexplorer() {
		//		System.setProperty("webdriver.ie.driver", "C:\\devapps\\chromedriver\\chromedriver.exe")
		//		return new InternetExplorerDriver()
		throw new UnsupportedOperationException("Internet Explorer is not supported yet...")
	}
}
