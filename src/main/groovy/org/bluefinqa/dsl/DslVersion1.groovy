package org.bluefinqa.dsl

import static org.junit.Assert.*
import groovy.json.JsonBuilder
import groovy.transform.ToString

import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.bluefinqa.utils.SelectorUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.openqa.selenium.By
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

@ToString(includeNames = true, includeSuper = true, ignoreNulls = true)
abstract class DslVersion1 extends BaseDsl {
	static final int WAIT_TIMEOUT = 10

	//
	// DSL
	//

	void open(String url) {
		assertNotNull "No open browser found", driver

		String finalUrl = null

		if (config.baseUrl?.endsWith("/")) {
			config.baseUrl = config.baseUrl.substring(0, config.baseUrl.length() - 1);
		}

		if (url.startsWith('http://') || url.startsWith('https://')) {
			finalUrl = url
		} else if (config.baseUrl) {
			finalUrl = config.baseUrl + url
		}

		assertNotNull "Invalid URL to open", finalUrl

		logDebug "Opening '${finalUrl}' [baseUrl = '${config.baseUrl}', path = '${url}']"

		driver.get(finalUrl)
	}

	void click(def arg) {
		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		def elements = findAllElementsBy(by)
		assertTrue "Expected element was not found", elements.size() > 0

		elements.each {
			logDebug 'Clicking element: ' + it
			it.click()
		}
	}

	void clear(def arg) {
		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		WebElement e = findOneElementBy(by)
		assertNotNull "Expected element was not found", e

		e.clear()
	}

	void type(Map arg, String keys) {
		assertNotNull "Cannot type null value", keys

		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		WebElement e = findOneElementBy(by)
		assertNotNull "Expected element was not found", e

		e.sendKeys(keys)
	}

	//
	// Asserts
	//

	void assertTitle(String title) {
		assertEquals title, driver.title
	}

	void assertNotTitle(String title) {
		assertNotEquals title, driver.title
	}

	void assertTextPresent(String text) {
		String content = driver.findElement(By.tagName("body")).getText()
		assertTrue "Page lacks expected text '${text}'", content.toLowerCase().contains(text.toLowerCase())
	}

	void assertTextNotPresent(String text) {
		String content = driver.findElement(By.tagName("body")).getText()
		assertFalse "Page contains unexpected text '${text}'", content.toLowerCase().contains(text.toLowerCase())
	}

	void assertElementPresent(def arg) {
		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by
		assertNotNull "Failed to locate element " + by, findOneElementBy(by)
	}

	void assertElementNotPresent(def arg) {
		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by
		assertNull "Unexpected element found " + by, findOneElementBy(by)
	}

	void assertElementContainsText(Map arg, String text) {
		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		def el = findOneElementBy(by)
		assertNotNull "Failed to locate element " + by, el
		assertTrue "Element lacks expected text '${text}'", el.getText().toLowerCase().contains(text.toLowerCase())
	}

	void assertElementDisabled(def arg) {
		def e = findOne(arg)
		def disabled = e.getAttribute('disabled') as boolean
		assertTrue "Element should be disabled", disabled
	}

	void assertElementEnabled(def arg) {
		def e = findOne(arg)
		def disabled = e.getAttribute('disabled') as boolean
		assertFalse "Element should be enabled", disabled
	}

	void assertHttpCode(WebElement e, int code) {
		assertEquals 'Cannot assert http code on non a tags', 'a', e.tagName?.toLowerCase()
		def href = e.getAttribute('href')
		logDebug('href: ' + href)
		if (href.startsWith('http')) {
			CloseableHttpResponse response = httpClient.execute(new HttpGet(href));
			logDebug('Status Line: ' + response.getStatusLine())
			assertEquals 'HTTP Status Code doesn\'t match', code, response.getStatusLine().getStatusCode()
			response.close()
		} else {
			log.warn('Skipping ' + href)
		}
	}

	//
	// Wait
	//

	void waitTillElementTextPresent(Map arg, String text) {
		assertNotNull "No open browser found", driver

		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		try {
			(new WebDriverWait(driver, WAIT_TIMEOUT)).until(ExpectedConditions.textToBePresentInElement(by, text))
		} catch (TimeoutException e) {
			fail "Failed to locate expected text '${text}' within ${WAIT_TIMEOUT} seconds"
		}
	}

	void waitTillElementPresent(Map arg) {
		assertNotNull "No open browser found", driver

		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		try {
			(new WebDriverWait(driver, WAIT_TIMEOUT)).until(ExpectedConditions.presenceOfElementLocated(by))
		} catch (TimeoutException e) {
			fail "Failed to locate expected element '${by}' within ${WAIT_TIMEOUT} seconds"
		}
	}

	//
	// Helpers
	//

	List<WebElement> find(def arg) {
		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		findAllElementsBy(by)
	}

	WebElement findOne(def arg) {
		def by = SelectorUtils.getByFromSelectorMap(arg)
		assertNotNull "Invalid element selector", by

		findOneElementBy(by)
	}

	String html(def arg) {
		assertElementPresent arg
		def e = findOne(arg)
		e.getAttribute('outerHTML')
	}

	String text(def arg) {
		assertElementPresent arg
		def e = findOne(arg)
		e.getText()
	}

	Document parse(def arg) {
		Jsoup.parse(html(arg))
	}

	String json(def map) {
		new JsonBuilder(map).toString()
	}

	String getPageTitle() {
		driver?.getTitle()
	}
}
