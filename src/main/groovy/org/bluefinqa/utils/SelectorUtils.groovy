package org.bluefinqa.utils

import org.openqa.selenium.By

abstract class SelectorUtils {
	static By getByFromSelectorMap(def arg) {
		if (arg != null && arg instanceof Map) {
			if (arg.id != null) {
				return By.id(arg.id)
			} else if (arg.className != null) {
				return By.className(arg.className)
			} else if (arg.css != null) {
				return By.cssSelector(arg.css)
			} else if (arg.linkText != null) {
				return By.linkText(arg.linkText)
			} else if (arg.href != null) {
				return By.cssSelector('[href="' + arg.href + '"]')
			} else if (arg.xpath != null) {
				return By.xpath(arg.xpath)
			} else if (arg.action != null) {
				return By.cssSelector('[action="' + arg.action + '"]')
			} else if (arg.name != null) {
				return By.cssSelector('[name="' + arg.name + '"]')
			} else if (arg.value != null) {
				return By.cssSelector('[value="' + arg.value + '"]')
			} else if (arg.buttonText != null) {
				return By.xpath('//button[text()="' + arg.buttonText + '"]')
			}
		}
		return null
	}
}
