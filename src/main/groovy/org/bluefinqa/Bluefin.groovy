package org.bluefinqa;

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang3.StringUtils
import org.bluefinqa.dsl.BaseDsl
import org.bluefinqa.dsl.DslVersion1
import org.bluefinqa.dsl.enums.OnOff
import org.bluefinqa.dsl.enums.WebDriverType
import org.bluefinqa.dsl.enums.sets.KeywordSet1
import org.bluefinqa.utils.ExceptionUtils
import org.codehaus.groovy.control.CompilerConfiguration
import org.codehaus.groovy.control.customizers.ImportCustomizer
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import org.junit.Assert
import org.openqa.selenium.firefox.FirefoxDriver

class Bluefin {

	final static CFG_FILE_NAME = "bluefin.properties"

	static void main(def args) throws IOException {
		def cli = new CliBuilder(usage: 'bluefin [options] <path to tests>')

		cli.with {
			h longOpt: 'help', 'Show this message and exit'
			v longOpt: 'version', 'Show version'
		}

		def options = cli.parse(args)
		if (!args || options.h) {
			cli.usage()
			return
		}

		if (options.v) {
			version()
			return
		}

		def tests = null
		def path = args.last()
		def testsFile = new File(path)
		if (!testsFile.exists()) {
			error "Cannot access '${path}'"
			return
		}

		def props = new Properties()
		props.setProperty('baseUrl', '')
		def config =  new ConfigSlurper().parse(props)

		def cfgFile = null
		if (testsFile.isDirectory()) {
			tests = getTests(testsFile)
			cfgFile = new File(CFG_FILE_NAME, testsFile)
		} else {
			tests = [testsFile]
			cfgFile = new File(CFG_FILE_NAME, testsFile.parentFile)
		}

		if (cfgFile?.exists()) {
			def newprops = new Properties()
			cfgFile.withInputStream { newprops.load(it) }
			config = new ConfigSlurper().parse(newprops)
		}

		if (tests) {
			long ts0 = System.currentTimeMillis()
			int n = tests.size()

			int executed = 0
			int passed = 0
			int failed = 0
			int errored = 0
			int skipped = 0

			GroovyShell shell = new GroovyShell(Bluefin.class.getClassLoader(), new Binding(), configuration())

			def now = new Date().format("yyyy-MM-dd HH:mm:ss")

			println "#############################################################"
			println "#"
			println "#  Running tests: '${path}'"
			println "#  Date: ${now}"
			println "#"
			println "#############################################################"

			for (File f : tests) {
				String testName = f.getName()

				println "------------------- ${testName} -------------------"

				if (!skip(testName)) {
					long ts00 = System.currentTimeMillis()

					AssertionError assertError = null
					RuntimeException runtimeError = null

					BaseDsl script = null
					def run = null
					try {
						script = shell.parse(f)
						script.binding = binding()
						script.invokeMethod("setConfig", config)
						script.run()
					} catch (AssertionError e) {
						assertError = e
					} catch (RuntimeException e) {
						runtimeError = e
					}

					String status = "PASSED!"
					if (assertError != null || runtimeError != null) {
						status = "FAILED!"

						if (assertError != null) {
							int line = ExceptionUtils.getLineNumber(assertError, f.getName())
							println "!! assert failed: ${assertError.message} -> (${testName}:${line})";
							failed++
						} else if (runtimeError != null) {
							int line = ExceptionUtils.getLineNumber(runtimeError, f.getName())
							println "!! runtime error: ${runtimeError.message} (${testName}:${line})"
							errored++
						}

						script.cleanUp()
					} else {
						passed++
					}

					long ts01 = System.currentTimeMillis()
					long e00 = (ts01 - ts00)

					println ""
					println "${status} Time: ${e00} ms"
					println ""

					executed++
				} else {
					println "SKIPPED!"
					skipped++
				}
			}

			long ts1 = System.currentTimeMillis()
			long e0 = (ts1 - ts0)

			println "=============================================================================="
			if (failed > 0 || errored > 0) {
				println "FAILED!"
			} else {
				println "PASSED!"
			}
			println "Total tests: ${n} (executed: ${executed}, skipped: ${skipped}, passed: ${passed}, failed: ${failed}, with errors: ${errored})"
			println ""
			println "Total time: ${e0} ms"
			println ""
		} else {
			println "No tests to run for '${path}'."
		}
	}

	static boolean skip(String test) {
		test.endsWith("__skip.groovy")
	}

	static ImportCustomizer imports() {
		ImportCustomizer imports = new ImportCustomizer()

		//
		// Regular
		//

		// local
		def localImports = [DslVersion1.class]

		localImports.each {
			imports.addImports(it.getCanonicalName())
		}


		// 3rd party
		def externalImports = [
			Document.class,
			Elements.class,
			FirefoxDriver.class
		]

		externalImports.each {
			imports.addImports(it.getCanonicalName())
		}

		//
		// Static
		//
		def staticImports = [
			Assert.class,
			WebDriverType.class,
			OnOff.class,
			KeywordSet1.class,
		]

		staticImports.each {
			imports.addStaticStars(it.getCanonicalName())
		}

		imports
	}

	static CompilerConfiguration configuration() {
		CompilerConfiguration conf = new CompilerConfiguration()

		conf.setScriptBaseClass(DslVersion1.class.getName())
		conf.addCompilationCustomizers(imports())

		conf
	}

	static Binding binding() {
		new Binding()
	}

	static void error(String msg) {
		println "Error: ${msg}"
	}

	//TODO make this groovier
	static List<File> getTests(File dir) {
		File[] selectedFiles = dir.listFiles(new FileFilter() {
					boolean accept(File pathname) {
						if (StringUtils.endsWith(pathname.getName(), ".groovy")) {
							return true
						}
						return false
					}
				})

		Arrays.asList(selectedFiles)
	}

	static void version() {
		def version = Bluefin.class.getClassLoader().getResourceAsStream("version.properties").text
		println "Bluefin v${version}"
	}
}
